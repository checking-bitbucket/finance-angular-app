// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // tradeBackEndUrl: 'http://localhost:8080/api/trades/',
  // tradeBackEndUrl: "http://punedevopsa48.conygre.com:8081/api/trades/",
  tradeBackEndUrl:"http://finance-finance.punedevopsa48.conygre.com/api/trades/",
  helperApiBaseUrl:
    'https://api.polygon.io/v3/reference/tickers?active=true&sort=ticker&order=asc&limit=100&apiKey=U0qIz7vjDtRwfH9RaQCAi2dQqiISZzUL',
  tickerDetailApi: 'https://api.polygon.io/vX/reference/tickers/',
  tradeGraphApi: 'https://api.polygon.io/v2/aggs/ticker/',
  adviceApiUrl:"https://qz4sxjl623.execute-api.us-east-1.amazonaws.com/default/tradeAdvisor?ticker=",
  tickerLivePrice:"https://api.polygon.io/v2/aggs/ticker/"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
