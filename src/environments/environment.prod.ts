export const environment = {
  production: true,
  tradeBackEndUrl:"http://finance-finance.punedevopsa48.conygre.com/api/trades/",
  helperApiBaseUrl:
    'https://api.polygon.io/v3/reference/tickers?active=true&sort=ticker&order=asc&limit=100&apiKey=U0qIz7vjDtRwfH9RaQCAi2dQqiISZzUL',
  tickerDetailApi: 'https://api.polygon.io/vX/reference/tickers/',
  tradeGraphApi: 'https://api.polygon.io/v2/aggs/ticker/',
  adviceApiUrl:"https://qz4sxjl623.execute-api.us-east-1.amazonaws.com/default/tradeAdvisor?ticker=",
  tickerLivePrice:"https://api.polygon.io/v2/aggs/ticker/"
};
