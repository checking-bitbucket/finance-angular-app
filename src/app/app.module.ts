import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateTradeComponent } from './trade/create-trade/create-trade.component';
import { TradeListComponent } from './trade/trade-list/trade-list.component';
import { TradeElementComponent } from './trade/trade-element/trade-element.component';
import { HttpClientModule } from '@angular/common/http';
import { EditTradeComponent } from './trade/edit-trade/edit-trade.component';
import { FormsModule } from '@angular/forms';
import { TickerListComponent } from './trade/ticker-list/ticker-list.component';
import { TickerDetailsComponent } from './trade/ticker-details/ticker-details.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { TradeGraphComponent } from './trade/trade-graph/trade-graph.component';
import {
  ChartModule,
  RangeNavigatorModule,
  StockChartAllModule,
  ChartAllModule,
} from '@syncfusion/ej2-angular-charts';
import {
  AreaSeriesService,
  DateTimeService,
  RangeTooltipService,
} from '@syncfusion/ej2-angular-charts';
import { FilterTickersPipe } from './pipes/filter-tickers.pipe';
import { TradeFilterPipe } from './pipes/trade-filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CreateTradeComponent,
    TradeListComponent,
    TradeElementComponent,
    EditTradeComponent,
    TickerListComponent,
    TickerDetailsComponent,
    TradeGraphComponent,
    FilterTickersPipe,
    TradeFilterPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    ChartModule,
    RangeNavigatorModule,
    StockChartAllModule,
    ChartAllModule,
  ],
  providers: [AreaSeriesService, DateTimeService, RangeTooltipService],
  bootstrap: [AppComponent],
})
export class AppModule {}
