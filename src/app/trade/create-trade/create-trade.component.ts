import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelperApiService } from 'src/app/shared/helper-api.service';
import { Trade } from 'src/app/shared/trade';
import { TradeApiService } from 'src/app/shared/trade-api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-trade',
  templateUrl: './create-trade.component.html',
  styleUrls: ['./create-trade.component.css'],
})
export class CreateTradeComponent implements OnInit {
  public trade: Trade;
  public tickerDetail: any;
  public livePrice:any;

  constructor(private tradeApi: TradeApiService, private router: Router,private helperApi: HelperApiService,
    private route: 
    ActivatedRoute) {
    this.trade = new Trade();
    this.tickerDetail={};
  }

  ngOnInit(): void {
    const tradeId = this.route.snapshot.paramMap.get('id');
    this.loadTickers(tradeId);
    this.getLivePrice(tradeId);
  }

  setTradePrice(price: number) {
    this.trade.price = price;
  }

  setTradeVolume(volume: number) {
    this.trade.volume = volume;
  }

  createTrade() {
    this.tradeApi
      .createTrade(this.trade)
      .subscribe((data: {}) => this.router.navigate(['/trade-list']));
  }

  loadTickers(id:any){
   this.helperApi.getTickerDetail(id)
                  .subscribe((data:any) =>{
                                    this.tickerDetail=data.results;
                                    this.trade.stockTicker=this.tickerDetail.ticker;
                                  });
   console.log("Here is list of tickers",this.tickerDetail);
  }

  getLivePrice(tradeId:any){
    this.helperApi.getLivePrice(tradeId).subscribe((data:any) => this.livePrice =data.results[0]['c']);
    console.log("Closing price is:", this.livePrice);
  }
}
