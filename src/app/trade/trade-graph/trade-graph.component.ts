import { Component, OnInit } from '@angular/core';

// import { chartData } from './datasource';
import {
  DateTime,
  AreaSeries,
  CandleSeries,
  HiloOpenCloseSeries,
  HiloSeries,
  LineSeries,
  SplineSeries,
} from '@syncfusion/ej2-charts';
import {
  AccumulationDistributionIndicator,
  AtrIndicator,
  BollingerBands,
  EmaIndicator,
  MomentumIndicator,
} from '@syncfusion/ej2-charts';
import {
  MacdIndicator,
  RsiIndicator,
  Trendlines,
  SmaIndicator,
  StochasticIndicator,
} from '@syncfusion/ej2-charts';
import {
  TmaIndicator,
  RangeTooltip,
  Tooltip,
  Crosshair,
  ITooltipRenderEventArgs,
  IStockChartEventArgs,
  ChartTheme,
} from '@syncfusion/ej2-charts';

import { HelperApiService } from 'src/app/shared/helper-api.service';
import { TradeData } from 'src/app/shared/trade-data';
import { leadingComment } from '@angular/compiler';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-trade-graph',
  // templateUrl: './trade-graph.component.html',
  // styleUrls: ['./trade-graph.component.css'],
  template:
`<ejs-stockchart id="chart-container"  [title]='title' width='650' height='350' [crosshair]='crosshair'   style="display:block;" [primaryXAxis]='primaryXAxis'  
      [indicatorType]='indicatorType'  [tooltip]='tooltip'>
   <e-stockchart-series-collection>
   <e-stockchart-series [dataSource]='chartData' type='Candle' xName='date' High='high' Low='low' Open='open' Close ='close' Name='Apple'></e-stockchart-series>
    </e-stockchart-series-collection>
</ejs-stockchart>`
})
export class TradeGraphComponent implements OnInit {
  constructor(
    private helperApi: HelperApiService,
    private route: ActivatedRoute
  ) {}

  // ngOnInit(): void {
  // }

  public primaryXAxis!: Object;
  public primaryYAxis!: Object;
  public chartData!: Object;
  public title!: string;
  public crosshair!: Object;
  public tickerId!: any;

public indicatorType: string[] = [];

public enable: boolean = true;
public tooltip!: Object;
  // public remoteData!:Object;

  ngOnInit(): void {
    this.tickerId = this.route.snapshot.paramMap.get('id');
    this.loadData();
    // this.chartData = this.remoteData;
    this.title = 'Stock trends over time';
    this.primaryXAxis = {
      valueType: 'DateTime',
      crosshairTooltip: { enable: true },
    };
    this.primaryYAxis = {
      majorTickLines: { color: 'transparent', width: 0 },
      crosshairTooltip: { enable: true },
    };
    this.crosshair = {
      enable: true,
    };
    this.tooltip = { enable: true };
  }

  formatDate(date: any) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  // console.log(formatDate('Sun May 11,2014'));

  loadData() {
    return this.helperApi
      .getTradeGraphData(this.tickerId)
      .subscribe((data: any) => {
        this.chartData = data.results.map((elem: any) => ({
          date: new Date(this.formatDate(elem.t)),
          open: elem.o,
          close: elem.c,
          high: elem.h,
          low: elem.l,
          volume: elem.v,
        }));
        console.log('This is the data', this.chartData);
      });
  }
}
