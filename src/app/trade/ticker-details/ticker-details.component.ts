import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperApiService } from 'src/app/shared/helper-api.service';
import { AdviceApiService } from 'src/app/shared/advice-api.service';

@Component({
  selector: 'app-ticker-details',
  templateUrl: './ticker-details.component.html',
  styleUrls: ['./ticker-details.component.css'],
})
export class TickerDetailsComponent implements OnInit {
  public ticker: any = {};
  public tickerKeys: any = [];
  public advice:string="";
  public advice_json:any={};
  public field_advice:string="";
  public mandatory_keys: any = [
    'ceo',
    'country',
    'description',
    'exchange',
    'industry',
    'marketcap',
    'name',
    'phone',
    'sector',
  ];
  

  constructor(
    private helperApi: HelperApiService,
    private route: ActivatedRoute,
    public router: Router,
    private adviceApi:AdviceApiService
  ) {}

  tickerId = this.route.snapshot.paramMap.get('id');

  ngOnInit(): void {
    
    this.helperApi.getTickerDetail(this.tickerId).subscribe((ticker) => {
      this.ticker = ticker.results;
      this.tickerKeys = Object.keys(this.ticker);
    });

    this.getTickerAdvice();
  }

  splitstring(advice_string:string){
    var advice_arr_1;
    var advice_arr_2;
    advice_arr_1=advice_string.split(",");
    advice_arr_2=advice_arr_1[4].split(":");
    advice_arr_2[1]=advice_arr_2[1].slice(0,-1);
    return advice_arr_2[1];

  }

  getTickerAdvice(){
    return this.adviceApi.getAdvice(this.tickerId).subscribe((data)=>{
      this.advice=data;
      //this.advice_json=JSON.parse(this.advice);
      this.field_advice=this.splitstring(this.advice);
      
    })
}
}
