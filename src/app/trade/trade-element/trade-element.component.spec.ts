import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeElementComponent } from './trade-element.component';

describe('TradeElementComponent', () => {
  let component: TradeElementComponent;
  let fixture: ComponentFixture<TradeElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradeElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
