import { Component, OnInit } from '@angular/core';
import { TradeApiService } from 'src/app/shared/trade-api.service';

@Component({
  selector: 'app-trade-element',
  templateUrl: './trade-element.component.html',
  styleUrls: ['./trade-element.component.css'],
})
export class TradeElementComponent implements OnInit {
  Trades: any = [];
  p: number = 1;
  searchText:string="";
  constructor(public tradeApi: TradeApiService) {}

  ngOnInit(): void {
    this.loadTrades();
  }

  loadTrades() {
    return this.tradeApi.getTrades().subscribe((data: {}) => {
      console.log(data);
      this.Trades = data;
    });
  }

  deleteTrade(id: any) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.tradeApi.deleteTrade(id).subscribe((data) => {
        this.loadTrades();
      });
    }
  }

  getTrade(id: number) {
    this.tradeApi.getTrade(id).subscribe((data: {}) => {
      const indx = this.Trades.findIndex((trade: any) => trade.id === id);
      this.Trades[indx] = data;
    });
  }

  getStatusPhrase(code: any) {
    switch (code) {
      case 0:
        return 'Initial State';
        break;
      case 1:
        return 'Processing';
        break;
      case 2:
        return 'Success';
        break;
      default:
        return 'Failed';
    }
  }
}
