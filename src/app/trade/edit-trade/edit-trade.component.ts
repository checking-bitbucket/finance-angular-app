import { Component, OnInit } from '@angular/core';
import { TradeApiService } from 'src/app/shared/trade-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Trade } from 'src/app/shared/trade';

@Component({
  selector: 'app-edit-trade',
  templateUrl: './edit-trade.component.html',
  styleUrls: ['./edit-trade.component.css'],
})
export class EditTradeComponent implements OnInit {
  public trade!: Trade;
  constructor(
    private tradeApi: TradeApiService,
    private route: ActivatedRoute,
    public router: Router
  ) {}

  ngOnInit(): void {
    const tradeId = this.route.snapshot.paramMap.get('id');
    this.tradeApi.getTrade(tradeId).subscribe((trade) => (this.trade = trade));
  }

  setTradePrice(price: number) {
    this.trade.price = price;
  }

  setTradeVolume(volume: number) {
    this.trade.volume = volume;
  }

  editTrade(id: number) {
    this.tradeApi
      .updateTrade(id, this.trade)
      .subscribe((data: {}) => this.router.navigate(['/trade-list']));
  }
}
