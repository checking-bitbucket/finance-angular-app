import { Component, OnInit } from '@angular/core';
import { HelperApiService } from 'src/app/shared/helper-api.service';

@Component({
  selector: 'app-ticker-list',
  templateUrl: './ticker-list.component.html',
  styleUrls: ['./ticker-list.component.css'],
})
export class TickerListComponent implements OnInit {
  tickers: any = [];
  p: number = 1;
  searchText:string="";

  constructor(private helperApi: HelperApiService) {}

  ngOnInit(): void {
    this.loadTickers();
  }

  loadTickers() {
    return this.helperApi.getTickers().subscribe((data) => {
      this.tickers = data.results;
      console.log(data.results);
    });
  }
}
