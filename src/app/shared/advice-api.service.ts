import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {environment} from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AdviceApiService {

  apiURL = environment.adviceApiUrl;
  
  constructor(private http: HttpClient) { }
  

  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',    
    })
  }
  

  getAdvice(tickername:any):Observable<any>{
      return this.http.get(this.apiURL+tickername,{responseType:'text'})
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
