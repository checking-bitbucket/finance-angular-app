import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Trade } from './trade';

@Injectable({
  providedIn: 'root',
})
export class TradeApiService {
  apiURL = environment.tradeBackEndUrl;

  constructor(private http: HttpClient) {}

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  // HttpClient API get() method
  getTrades(): Observable<Trade> {
    return this.http
      .get<Trade>(this.apiURL)
      .pipe(retry(1), catchError(this.handleError));
  }

  // HttpClient API get() method
  getTrade(id: any): Observable<Trade> {
    return this.http
      .get<Trade>(this.apiURL + id)
      .pipe(retry(1), catchError(this.handleError));
  }

  // HttpClient API post() method
  createTrade(trade: Trade): Observable<Trade> {
    return this.http
      .post<Trade>(this.apiURL + '', JSON.stringify(trade), this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  // HttpClient API put() method
  updateTrade(id: number, trade: Trade): Observable<Trade> {
    return this.http
      .put<Trade>(this.apiURL + id, JSON.stringify(trade), this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  // HttpClient API delete() method
  deleteTrade(id: number) {
    return this.http
      .delete<Trade>(this.apiURL + id, this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  // Error handling
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
