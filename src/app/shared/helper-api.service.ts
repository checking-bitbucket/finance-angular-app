import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CoreEnvironment } from '@angular/compiler/src/compiler_facade_interface';
@Injectable({
  providedIn: 'root',
})
export class HelperApiService {
  apiURL = environment.helperApiBaseUrl;
  specialApiUrl = environment.tickerDetailApi;
  graphDataUrl = environment.tradeGraphApi;
  livePriceUrl=environment.tickerLivePrice;

  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  getTickers(): Observable<any> {
    return this.http
      .get<any>(this.apiURL, this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  getTickerDetail(id: any): Observable<any> {
    return this.http
      .get<any>(
        this.specialApiUrl + id + '?&apiKey=U0qIz7vjDtRwfH9RaQCAi2dQqiISZzUL'
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  getTradeGraphData(tickerId: any) {
    return this.http
      .get<any>(
        this.graphDataUrl +
          tickerId +
          '/range/1/day/2011-08-29/2021-08-29?adjusted=true&sort=asc&limit=3650&apiKey=lVwoOebXzhCXz4fwY6XbcdcC4yxevcXF'
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  getLivePrice(id:any){
    return this.http
            .get<any>(
              this.livePriceUrl + id + '/prev?adjusted=true&apiKey=lVwoOebXzhCXz4fwY6XbcdcC4yxevcXF'
            ).pipe(retry(1), catchError(this.handleError));
  }
}
