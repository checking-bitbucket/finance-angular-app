export class Trade {
  id: number;
  stockTicker: string;
  price: number;
  volume: number;
  statusCode: number;
  buyOrSell: string;
  timestamp!: string;

  constructor() {
    this.id = 0;
    this.stockTicker = '';
    this.price = 0;
    this.volume = 0;
    this.statusCode = 0;
    this.buyOrSell = '';
  }
}
