export class TradeData {
  date: Date;
  high: number;
  low: number;
  close: number;
  volume: number;

  constructor() {
    this.date = new Date();
    this.high = 0;
    this.low = 0;
    this.close = 0;
    this.volume = 0;
  }
}
