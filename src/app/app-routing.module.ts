import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateTradeComponent } from './trade/create-trade/create-trade.component';
import { EditTradeComponent } from './trade/edit-trade/edit-trade.component';
import { TickerDetailsComponent } from './trade/ticker-details/ticker-details.component';
import { TickerListComponent } from './trade/ticker-list/ticker-list.component';
import { TradeElementComponent } from './trade/trade-element/trade-element.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'trade-list' },
  { path: 'create-trade', component: CreateTradeComponent },
  { path: 'create-trade/:id', component: CreateTradeComponent },
  { path: 'trade-list', component: TradeElementComponent },
  {path: 'edit-trade/:id', component: EditTradeComponent},
  {path: 'ticker-list', component: TickerListComponent},
  {path: 'ticker-details/:id', component: TickerDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
